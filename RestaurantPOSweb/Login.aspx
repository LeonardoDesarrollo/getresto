﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" EnableEventValidation="false" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login | Point of sale System</title>
    <%-- <link href="BootStrapFiles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="BootStrapFiles/signin.css" rel="stylesheet" type="text/css" />--%>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" >
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <style>
        body {
            padding-top: 4.2rem;
            padding-bottom: 4.2rem;
            background: rgba(0, 0, 0, 0.76);
        }

        a {
            text-decoration: none !important;
        }

        h1, h2, h3 {
           
        }

        .myform {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            padding: 1rem;
            -ms-flex-direction: column;
            flex-direction: column;
            width: 100%;
            pointer-events: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0,0,0,.2);
            border-radius: 1.1rem;
            outline: 0;
            max-width: 500px;
        }

        .tx-tfm {
            text-transform: uppercase;
        }

        .mybtn {
            border-radius: 50px;
        }

        .login-or {
            position: relative;
            color: #aaa;
            margin-top: 10px;
            margin-bottom: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .span-or {
            display: block;
            position: absolute;
            left: 50%;
            top: -2px;
            margin-left: -25px;
            background-color: #fff;
            width: 50px;
            text-align: center;
        }

        .hr-or {
            height: 1px;
            margin-top: 0px !important;
            margin-bottom: 0px !important;
        }

        .google {
            color: #666;
            width: 100%;
            height: 40px;
            text-align: center;
            outline: none;
            border: 1px solid lightgrey;
        }

        form .error {
            color: #ff0000;
        }

        #second {
            display: none;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <div class="container">
            <div class="row">
                <div class="col-md-5 mx-auto">
                    <div id="first">
                        <div class="myform form ">
                            <div class=" mb-3">
                                <div class="col-md-12 text-center">
                                    <h1>GetFenden</h1>
                                    <h4>Punto de Venta</h4>
                                </div>
                            </div>
                            <form action="" method="post" name="login">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Usuario</label>
                                    <asp:TextBox ID="txtuser" runat="server" type="text" class="form-control" placeholder="Usuario" required autofocus></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Password</label>
                                    <asp:TextBox ID="txtpass" runat="server" class="form-control" placeholder="Password" required TextMode="Password"></asp:TextBox>
                                </div>

                                <div class="col-md-12 text-center ">
                                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                        ErrorMessage="*" ControlToValidate="txtuser"></asp:RequiredFieldValidator>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Ingresar"
                                        class="btn btn-block mybtn btn-primary tx-tfm" OnClick="btnSubmit_Click" />
                                    <asp:Label ID="lblLogMsg" runat="server" ForeColor="Red" Text="---------"></asp:Label>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="login-or">
                                        <hr class="hr-or">
                                        <span class="span-or">or</span>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <p class="text-center">
                                        <a href="javascript:void();" class="google btn mybtn"><i class="fa fa-pencil"></i>Registrarte
                                 </a>
                                    </p>
                                </div>
                              
                            </form>

                        </div>
                    </div>
                  
                </div>
            </div>
        </div>








      

        <%-- Traffic Statistics --%>
        <%--             <div class="col-lg-3 col-lg-offset-4" >
                  <a href="http://info.flagcounter.com/LId3">
                <img src="http://s05.flagcounter.com/count/LId3/bg_FFFFFF/txt_000000/border_CCCCCC/columns_4/maxflags_20/viewers_0/labels_1/pageviews_1/flags_0/" alt="Flag Counter" border="0">
                </a>
        </div>  --%>
    </form>

</body>
</html>
