﻿<%@ Page Title="" Language="C#" MasterPageFile="../MasterPage/Bootstrap.master" EnableViewState="true" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="SalesRegister.aspx.cs" Inherits="SalesRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="atk" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <%--/////////<div class="input-group input-group-sm">//// Item list --%>


    <%--    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <asp:Panel ID="panelItems" runat="server" CssClass="col-lg-7">
        <div class="panel  panel-success">
            <asp:Label ID="LblIdMesa" Visible="false" runat="server"></asp:Label>
            <asp:Label ID="LblNombreMesa" Visible="false" Font-Bold="true" Font-Size="X-Large" runat="server"></asp:Label>
            <%-----------------------------    Category --------------------------------Start--------- --%>
            <div class="panel-body">
                <asp:DataList ID="dtlistcategory" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="row">
                    <ItemStyle ForeColor="Black" />
                    <ItemTemplate>
                        <asp:LinkButton CssClass="btn btn-success" ID="lnkCategory" runat="server" Text='<%# Bind("ItemCategory") %>' OnClick="lnkCategory_Onclick" Font-Size="13" ForeColor="White"> </asp:LinkButton>
                        <asp:Label ID="lblcategid" Font-Size="13px" runat="server" Text='<%# Bind("ItemCategory") %>' Visible="False"></asp:Label>
                    </ItemTemplate>
                </asp:DataList>
            </div>
            <%----------------------------------------- Category END ---------------------------------------%>
            <asp:Panel ID="Panel1" runat="server" ScrollBars="Vertical" Height="445px">
                <br />
                <asp:DataList ID="DataList1" runat="server" Font-Names="Verdana" Font-Size="Small" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="row">
                    <ItemStyle ForeColor="Black" />
                    <ItemTemplate>
                        <div class="col-sm-3">
                            <div id="pricePlansmsg">
                                <ul id="plans">
                                    <li class="plan">
                                        <asp:LinkButton ID="LbtnArticulo" runat="server" OnClick="LbtnArticulo_Click" Font-Underline="false">


                                            <ul class="planContainer">
                                                <li class="title">
                                                    <asp:Label ID="LblID" Visible="false" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label ID="LblCode" Visible="false" runat="server" Text='<%# Eval("Code") %>'></asp:Label>
                                                    <asp:Label Visible="false" ID="Lbloptions" runat="server" Text='<%# Bind("options") %>'></asp:Label>
                                                    <asp:Label Visible="false" ID="Lbldescription" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                                    <asp:Label ID="LblItemName" Visible="false" runat="server" Text='<%# Eval("ItemName") %>'></asp:Label>
                                                    <asp:Label ID="LblQty" Visible="false" runat="server" Text='<%# Eval("Qty") %>'></asp:Label>
                                                    $<asp:Label ID="LblPrice" Visible="true" runat="server" Text='<%# Eval("Price","{0:n}") %>'></asp:Label>
                                                    <asp:Label ID="LblDisc" Visible="false" runat="server" Text='<%# Eval("Disc") %>'></asp:Label>
                                                    <asp:Label ID="LblTotal" Visible="false" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                    <asp:Label ID="Lblkditem" Visible="false" runat="server" Text='<%# Eval("kditem") %>'></asp:Label>
                                                    <h5>
                                                        <asp:Label ID="lblitemNametop" Font-Size="14px" Font-Bold="true" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label></h5>
                                                </li>
                                                <li class="title">
                                                    <asp:Image ID="imgPhoto" class="img-circle" runat="server" Width="80px" Height="80px" ImageUrl='<%# Eval("Photo")%>' />
                                                </li>
                                                <li>
                                                    <ul class="options">
                                                        <li><span>Precio:
                                                        <asp:Label ID="Label9" runat="server" Text='<%# Bind("Total") %>'></asp:Label>
                                                            (<asp:Label ID="Label8" ForeColor="Black" Font-Size="10px" runat="server" Text='<%# Bind("Qty") %>'></asp:Label>)</span>  </li>
                                                    </ul>
                                                </li>
                                                <asp:Button ID="btnPopuOptions" runat="server" Text="Agregar" ValidationGroup="vG2" Font-Size="12px" ToolTip="Agregar" class="btn btn-primary btn-xs" OnClick="btnPopuOptions_Goclick" />
                                                <br />
                                            </ul>
                                        </asp:LinkButton>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </asp:Panel>

        </div>
    </asp:Panel>

    <asp:Panel ID="paneCartlItemsFull" runat="server" CssClass="col-lg-5">
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-barcode"></span></span>
            <asp:TextBox ID="txtItemSearch" runat="server"
                placeholder="Buscar para escanear productos" class="form-control" ToolTip="Búsqueda por código de artículo o nombre de artículo"
                OnTextChanged="txtItemSearch_TextChanged" AutoPostBack="true"></asp:TextBox>
            <span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>
        </div>
        <asp:Panel ID="paneCartlItems" runat="server" class="panel panel-default" ScrollBars="Vertical" Height="340px">
            <%--  ScrollBars="Vertical" Height="340px"--%>  <%--Scroll Added cart Panel  --%>

            <asp:DataList ID="dtlistgrid" runat="server" Font-Names="Verdana" Font-Size="Small" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="row">
                <ItemStyle ForeColor="Black" />
                <ItemTemplate>
                    <div class="col-md-12">
                        <div id="pricePlansmsg">
                            <ul id="plans">
                                <li class="panel-body plan">
                                    <ul class="planContainer">
                                        <div class="col-md-2" style="text-align: left">
                                            <asp:Image ID="imgPhoto" runat="server" class="img-circle" Width="40px" Height="40px" ImageUrl='<%# Bind("image") %>' />

                                            <br />
                                        </div>
                                        <div class="col-lg-5" style="text-align: left">
                                            <asp:Label Visible="false" ID="lblid" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                                            <asp:Label ID="Lblkditem" Visible="false" runat="server" Text='<%# Eval("kditem") %>'></asp:Label>
                                            <asp:Label ForeColor="Black" ToolTip="Cantidad Item" Font-Bold="true" ID="Label1" runat="server" Text='<%# Bind("Qty") %>'></asp:Label>
                                            -                                                   
                                            <asp:Label ID="lblitemname" Font-Size="13px" runat="server" Text='<%# Bind("ItemName") %>' ForeColor="#0084B4"></asp:Label>
                                            <asp:Label ID="lblObservacion" Visible="false" runat="server" Text='<%# Eval("Observacion") %>'></asp:Label>
                                            <br />
                                            Precio:<asp:Label ID="LblPrice" runat="server" Text='<%# Eval("Price") %>'></asp:Label>
                                            - Descuento:<asp:Label ForeColor="Black" ToolTip="Descuento" ID="lblDisc" runat="server" Text='<%# Bind("Disc") %>'></asp:Label>% 
                                                       <br />
                                            <asp:Label ForeColor="Black" ToolTip="Opciones" ID="lblOptions" runat="server" Text='<%# Bind("Options") %>' Font-Size="8"></asp:Label>

                                        </div>
                                        <div class="col-md-3">
                                            <asp:Label ForeColor="Black" ToolTip="Precio" ID="lblTotal" runat="server" Text='<%# Bind("Total") %>' Font-Size="15px"></asp:Label>

                                        </div>
                                        <div class="col-md-2" style="text-align: right">
                                            <asp:LinkButton ID="LinkDele" runat="server" ForeColor="Red" Font-Size="20px" ToolTip="Eliminar item" class="glyphicon glyphicon-trash" OnClick="btnDeleteitem_Click" />
                                        </div>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
            <br />
        </asp:Panel>
        <div class="panel panel-default">
            <div class="panel-footer" style="text-align: right">
                Subtotal = 
                <asp:Label ID="lblsubTotal" runat="server" Font-Bold="true" Text="0"></asp:Label><br />
                <asp:Label ID="Label1" Font-Size="11px" runat="server" Text="Vat ="></asp:Label>
                (<asp:Label ID="lblVatRate" Font-Size="9px" runat="server" Text="0"></asp:Label>)
                <asp:Label ID="lblVat" Font-Size="11px" runat="server" Text="0"></asp:Label><br />
                Total =    
                <asp:Label ID="lbltotal" runat="server" Font-Bold="true" Text="0"></asp:Label>
                <br />

                <asp:Label ID="Label7" Font-Size="11px" runat="server" Text="Total Items"> </asp:Label>
                <asp:Label ID="lblTotalQty" Font-Size="11px" runat="server" Text="0"></asp:Label>
                <br />
                <br />

                <asp:Label ID="lbltableno1" runat="server" Text="Table:Null" Visible="false"></asp:Label>
                <asp:Button ID="btnchangetable" runat="server" class="btn btn-info" Text="Cambiar Mesa" PostBackUrl="~/Sales/Tableview.aspx" Visible="false" />
                <asp:Button ID="btnsuspen" runat="server" class="btn btn-danger" Text="Suspender" OnClick="btnsuspen_Click" />
                <asp:Button ID="btnPedirCuenta" runat="server" class="btn btn-danger" Text="Cuenta" OnClick="btnsuspen_Click" />
                <asp:Button ID="btnPayment" runat="server" class="btn btn-success" Text="Pagar" OnClick="btnPayment_Click" />
            </div>
        </div>
    </asp:Panel>



    <%--<<<<<<<<<<<<<<<<<<<<< ---------------   Options view      Start --------------    >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>--%>

    <!-- Modal -->
    <div class="modal  fade in" id="detailsmodal" role="dialog">
        <div class="modal-backdrop fade in" style="height: 100%;"></div>
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content col-lg-12">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">X</button>
                    <h4 class="modal-title">
                        <asp:Label ID="Label14" runat="server" Font-Size="15px" Text=" Seleccione sus opciones"></asp:Label><br />
                        <asp:Label ID="Label10" runat="server" ForeColor="Black" Font-Size="12px" Text="Necesita la cocina?:"></asp:Label>
                        <asp:Label ID="lblkditem" runat="server" ForeColor="Black" Font-Size="12px" Text="NO"></asp:Label>
                    </h4>
                </div>
                <div class="modal-body">
                    <asp:Panel ID="Panel2" runat="server" ScrollBars="Vertical" Height="338px">
                        <div class="col-md-7" style="text-align: left">
                            <asp:Label ID="lblItemname" Font-Bold="true" runat="server" Text="-"></asp:Label>
                            <br />
                            <asp:Label ID="lbldescriptionsPop" runat="server" ForeColor="#8e969d" Text="-" Font-Size="9"></asp:Label>
                        </div>
                        <div class="col-md-2">
                            Cantidad
                            <br />
                            <atk:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server"
                                TargetControlID="txtqty" Minimum="1" Maximum="999" Width="100" ViewStateMode="Enabled">
                            </atk:NumericUpDownExtender>
                            <asp:TextBox ID="txtqty" runat="server" Font-Size="14px" Text="1" Height="50px" Width="50px" ToolTip="Cantidad Item" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-3" style="text-align: right">
                            <br />
                            <asp:Label ID="lblitemPrice" Font-Bold="true" Font-Size="14px" runat="server" Text="0"></asp:Label>
                        </div>
                        <div class="col-md-12" style="text-align: left">
                            <br />
                            Opciones<br />
                            <asp:CheckBoxList ID="chkoptionslist" class="table-hover" runat="server" CellPadding="5"
                                CellSpacing="5" Font-Size="12" Visible="False" />
                        </div>
                        <div class="col-md-12" style="text-align: left">
                            <br />
                            Observación<br />
                            <asp:TextBox ID="txtObservacion" runat="server" CssClass="form-control teclado" TextMode="MultiLine" Height="100px"></asp:TextBox>

                        </div>
                    </asp:Panel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btncloseOptionsPopup" class="btn btn-danger btn-sm" runat="server" data-dismiss="modal" Text="Cerrar" />
                    <asp:Button ID="btn_GoAddtocart" class="btn btn-primary btn-sm" runat="server" Text="Agregar" OnClick="btn_Goclick" />

                </div>
            </div>

        </div>
    </div>

    <%--<<<<<<<<<<<<<<<<<<<<<END --------------- Details view   Popup  END -------------- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>--%>




    <%--<<<<<<<<<<<<<<<<<<<<< --------------- payment panel Popup -----------Start --- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>--%>
    <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
    <atk:ModalPopupExtender ID="ModalPopupPayment" runat="server" TargetControlID="btnShowPopup"
        PopupControlID="pnlpopupPayment" CancelControlID="btnClosePayment" BackgroundCssClass="modalBackground">
    </atk:ModalPopupExtender>

    <asp:Panel ID="pnlpopupPayment" class="panel panel-primary col-md-10" runat="server" BackColor="White" Style="display: none; text-align: left" DefaultButton="bntPay">

        <div class="row">
            <div class="col-md-12 text-center">
                <%--Mesa Seleccionada:--%>
                <asp:Label ID="lbltableno" runat="server" Text="Table-001" Visible="false"></asp:Label>
            </div>
        </div>

        <asp:Panel ID="pnlpopupPaymentbody" class="panel-body" runat="server" ScrollBars="Vertical" Height="100%">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <strong>Forma de Pago:</strong>
                            <asp:DropDownList ID="DDLPaidBy" runat="server" class="form-control" ClientIDMode="Static">
                                <asp:ListItem>Efectivo</asp:ListItem>
                                <asp:ListItem>Cheque</asp:ListItem>
                                <asp:ListItem>Tarjeta Debito</asp:ListItem>
                                <asp:ListItem>Tarjeta Credito</asp:ListItem>
                                <asp:ListItem>Paypal</asp:ListItem>
                                <asp:ListItem>Transferencia</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div  id="DivFormasDePago">
                                <button data-id="efectivo" type="button" class="btn btn-warning BtnFormaPago">Efectivo</button>
                                <button data-id="td" type="button" class="btn btn-info BtnFormaPago">Tarjeta Debito</button>
                                <button data-id="tc" type="button" class="btn btn-success BtnFormaPago">Tarjeta Credito</button>
                                <button data-id="cheque" type="button" class="btn btn-danger BtnFormaPago">Cheque</button>
                               <button data-id="tra" type="button" class="btn btn-primary BtnFormaPago">Transferencia</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <strong>Nota</strong>
                            <asp:Label ID="Label6" runat="server" Font-Size="8px" ToolTip="Opcional" Text="Opcional"></asp:Label>
                            <asp:TextBox ID="txtNote" runat="server" ToolTip="Opcional" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row hidden">
                        <strong>Seleccione Cliente:</strong>

                        <asp:DropDownList ID="DDLCustname" class="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDLCustname_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:Label Font-Size="1px" ID="lblCustID" runat="server" Text="0001"></asp:Label>
                        <asp:Label Font-Size="1px" ID="lblCustContact" runat="server" Text="121"></asp:Label>
                        <asp:Label Font-Size="1px" ID="lblcustaddress" runat="server" Text="-"></asp:Label>
                    </div>
                    <div class="row hidden">
                        <strong>Boleta Nº:</strong>
                        <asp:TextBox ID="txttokenno" runat="server" ToolTip="Tokenno" Text="1" class="form-control "></asp:TextBox>
                        <asp:RequiredFieldValidator ForeColor="Red" ControlToValidate="txttokenno" ValidationGroup="vr12" Font-Size="11px"
                            ID="RequiredFieldValidator2" runat="server" ErrorMessage="Ingrese el Nº de Boleta" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ForeColor="Red" Font-Size="12px" ValidationGroup="vr12"
                            ControlToValidate="txttokenno" ID="RegularExpressionValidator3"
                            runat="server" ErrorMessage="Nº Boleta acepta sólo valor numérico" ValidationExpression="[0-9]*\.?[0-9]*" SetFocusOnError="True">
                        </asp:RegularExpressionValidator>
                    </div>
                    <div class="row hidden">
                        <strong>Descuento</strong>
                        <asp:TextBox ID="txtdiscountamount" ToolTip="Descuento" Text="0" runat="server" class="form-control"
                            Placeholder="100" AutoPostBack="True" OnTextChanged="txtdiscountamount_TextChanged"></asp:TextBox>
                        <asp:RegularExpressionValidator ForeColor="Red" ValidationGroup="vr12"
                            ControlToValidate="txtdiscountamount" ID="RegularExpressionValidator2"
                            runat="server" ErrorMessage="Numaric value only" ValidationExpression="[0-9]*\.?[0-9]*" SetFocusOnError="True">
                        </asp:RegularExpressionValidator>
                    </div>



                    <div class="row">
                        <div class="col-md-12">
                            <strong>Pagado:</strong>

                            <div class="input-group">


                                <!-- /btn-group -->

                                <asp:TextBox ID="txtPaid" ClientIDMode="Static" ToolTip="Cantidad pagada por el cliente" runat="server" class="form-control input-lg"
                                    Placeholder="100" AutoPostBack="True" OnTextChanged="txtPaid_TextChanged"></asp:TextBox>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-lg agregar-pago" type="button" id="btnAgregarPago" >Agregar Pago</button>
                                </span>
                            </div>
                            <!-- /input-group -->


                            <asp:RequiredFieldValidator ForeColor="Red" ControlToValidate="txtPaid" ValidationGroup="vr12" Font-Size="11px"
                                ID="RequiredFieldValidator1" runat="server" ErrorMessage="Ingrese la cantidad pagada" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ForeColor="Red" Font-Size="11px" ValidationGroup="vr12" Display="Dynamic"
                                ControlToValidate="txtPaid" ID="RegularExpressionValidator1"
                                runat="server" ErrorMessage="Sólo valor numérico" ValidationExpression="[0-9]*\.?[0-9]*" SetFocusOnError="True">
                            </asp:RegularExpressionValidator>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <asp:Button ID="btncoin1" runat="server" class="btn btn-primary" Text="1" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum1_Click" />
                                        <asp:Button ID="btncoin2" runat="server" class="btn btn-primary" Text="2" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum2_Click" />
                                        <asp:Button ID="btnnum3" runat="server" class="btn btn-primary" Text="3" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum3_Click" />
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <asp:Button ID="btnnum4" runat="server" class="btn btn-primary" Text="4" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum4_Click" />
                                        <asp:Button ID="btnnum5" runat="server" class="btn btn-primary" Text="5" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum5_Click" />
                                        <asp:Button ID="btnnum6" runat="server" class="btn btn-primary" Text="6" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum6_Click" />
                                    </p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <asp:Button ID="btnnum7" runat="server" class="btn btn-primary" Text="7" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum7_Click" />
                                        <asp:Button ID="btnnum8" runat="server" class="btn btn-primary" Text="8" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum8_Click" />
                                        <asp:Button ID="btnnum9" runat="server" class="btn btn-primary" Text="9" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnnum9_Click" />
                                    </p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <asp:Button ID="btndot" runat="server" class="btn btn-success" Text="." Font-Bold="True" Font-Size="20" Visible="false" Width="90" OnClick="btndot_Click" />
                                        <asp:Button ID="btnZero" runat="server" class="btn btn-primary" Text="0" Font-Bold="True" Font-Size="20" Width="90" OnClick="btnZero_Click" />
                                        <asp:Button ID="btnClear" runat="server" class="btn btn-warning" Text="Limpiar" Font-Size="20" Width="180" OnClick="btnClear_Click" />
                                    </p>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <asp:Button ID="btnClosePayment" class="btn btn-danger btn-lg" runat="server" Text="Volver" Width="133" />
                                        <asp:Button ID="bntPay" class="btn btn-success btn-lg" runat="server" OnClick="bntPay_click" ValidationGroup="vr12" Text="Pagar" Width="133" />
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <asp:Button ID="Btn20000" runat="server" class="btn btn-success" Text="$20.000" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn20000_Click" />
                                    <asp:Button ID="Btn10000" runat="server" class="btn btn-success" Text="$10.000" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn10000_Click" />
                                    <asp:Button ID="Btn5000" runat="server" class="btn btn-success" Text="$5.000" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn5000_Click" />
                                    <asp:Button ID="Btn2000" runat="server" class="btn btn-success" Text="$2.000" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn2000_Click" />
                                    <asp:Button ID="Btn1000" runat="server" class="btn btn-success" Text="$1.000" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn1000_Click" />
                                </div>
                                <div class="col-md-6">
                                    <asp:Button ID="Btn500" runat="server" class="btn btn-warning" Text="$500" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn500_Click" />
                                    <asp:Button ID="Btn100" runat="server" class="btn btn-warning" Text="$100" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn100_Click" />
                                    <asp:Button ID="Btn50" runat="server" class="btn btn-warning" Text="$50" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn50_Click" />
                                    <asp:Button ID="Btn10" runat="server" class="btn btn-warning" Text="$10" Font-Bold="True" Font-Size="20" Width="150" OnClick="Btn10_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>--%>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="divFormaPago">
                                <table id="lista-forma-pago" class="u-full-width">
                                    <thead>
                                        <tr>
                                            <th>Forma de Pago</th>
                                            <th>Monto</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <a href="#" id="vaciar-forma-pago" class="button u-full-width">Eliminar todas</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <h2>Total a pagar:</h2>
                            <h2>Vuelto:</h2>
                            <h2>Debe:</h2>
                        </div>
                        <div class="col-md-4 text-right">

                            <h2 class="bg-primary">
                                <asp:Literal ID="lbltotalpay" runat="server" Text="-" ClientIDMode="Static"></asp:Literal></h2>
                            <h2 class="bg-primary">
                                <asp:Literal ID="lblChange" runat="server" Text="0" ClientIDMode="Static"></asp:Literal></h2>
                            <h2 class="bg-primary">
                                <asp:Literal ID="lblDue" runat="server" Text="0" ClientIDMode="Static"></asp:Literal></h2>
                        </div>
                    </div>
                </div>


            </div>


        </asp:Panel>
    </asp:Panel>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>


    <script type="text/javascript">



        


        

        function pageLoad() {

            const divFormaPago = document.querySelector('#divFormaPago');
            const vaciarFormaPagoBtn = document.querySelector('#vaciar-forma-pago');
            const btnAgregaPago = document.querySelector('#btnAgregarPago');
            //const btnFormaPago = document.querySelector(".BtnFormaPago");

            cargarEventListeners();
            function cargarEventListeners() {

                // Cuando agregas un curso presionando "Agregar al Carrito"
                btnAgregaPago.addEventListener('click', agregarFormaPago);

                // Elimina cursos del carrito
                divFormaPago.addEventListener('click', eliminarCurso);

                //btnFormaPago.addEventListener('click', seleccionaFormaPago);

                // Vaciar el carrito 
                vaciarFormaPagoBtn.addEventListener('click', () => {
                    articulosCarrito = []; // reseteamos el arreglo

                    limpiarHTML(); // Eliminamos todo el HTML
                })


            }

            $(".BtnFormaPago").on("click", function () {
                document.getElementById("DDLPaidBy").value = $(this).text();;
            });

        }
     
        function seleccionaFormaPago(e) {
            e.preventDefault();
            if (e.target.classList.contains('BtnFormaPago')) {
                
                const cursoSeleccionado = e.target;
                console.log(cursoSeleccionado);
            }
        }

        // 
        function agregarFormaPago(e) {
            e.preventDefault();
            console.log(e);
            //if (e.target.classList.contains('agregar-pago')) {
            //    const pagoSeleccionado = e.target.parentElement.parentElement;
            //    console.log(pagoSeleccionado);
            //    //leerDatosCurso(formaPagoSeleccionado);
            //}
        }

        function eliminarCurso(e) {
            e.preventDefault();
            
        }

        function leerDatosPago() {
            // Crear un objeto con el contenido del curso actual
            const infoPago = {
                formaPago: curso.querySelector('formaPago').textContent,
                monto: curso.querySelector('.monto span').textContent,
                id: curso.querySelector('a').getAttribute('data-id')
            }
        }

        const confirmar = document.querySelector('#txtPaid');
        confirmar.addEventListener('input', () => {

            let password = document.querySelector('#TxtPassword');
            let msj = document.querySelector('#MsjConfirmaPassword');

            if (password.value != confirmar.value) {
                document.querySelector('#BtnRegistrar').disabled = true;
                confirmar.classList.remove("is-valid");
                confirmar.classList.add("is-invalid");
                msj.innerHTML = "Ingrese la misma contraseña";
            } else {
                document.querySelector('#BtnRegistrar').disabled = false;
                confirmar.classList.remove("is-invalid");
                confirmar.classList.add("is-valid");
                //password.classList.remove("is-invalid");
                //password.classList.add("is-valid");
                msj.innerHTML = "";
            }
        })

    </script>

</asp:Content>

