﻿<%@ Page Title="Registration" Language="C#" MasterPageFile="MasterPage/Master_Ft_page.master" enableEventValidation="false" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="public_Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server"> 
    <div class="section-header">
        <h3>Abre tu cuenta</h3>
        <p>Es totalmente gratis usar - Selecciona tus comidas y delivery</p>
    </div>

    <div class="row" style="text-align:left">
        <div class="col-md-7">               
                    <asp:Label ID="Label37"   runat="server" Text="Name*"></asp:Label>
                    <asp:RequiredFieldValidator  ForeColor="Red" Font-Size="12px"  ControlToValidate="txtname" ValidationGroup="vlpgDetailsUR"  
                    ID="RequiredFieldValidator5" runat="server"   ErrorMessage="Favor ingresar el nombre completo" SetFocusOnError="True"></asp:RequiredFieldValidator>  
                    <asp:TextBox ID="txtname"  Placeholder="Nombre completo"  class="form-control"  runat="server" > </asp:TextBox>  

                    <asp:Label ID="Label3"    runat="server" Text="Telefono*"></asp:Label>
                    <asp:RequiredFieldValidator  ForeColor="Red"   Font-Size="12px" ControlToValidate="txtphone" ValidationGroup="vlpgDetailsUR"  
                    ID="RequiredFieldValidator3" runat="server"   ErrorMessage="Favor ingresar un teléfono" SetFocusOnError="True"></asp:RequiredFieldValidator>  
                    <asp:TextBox ID="txtphone"  Placeholder="*** *** ****"  class="form-control"  runat="server" > </asp:TextBox> 

                    <asp:Label ID="Label1" runat="server"   Text="Nombre Usuario:*"></asp:Label>
                    <asp:RequiredFieldValidator  ForeColor="Red"  Font-Size="12px"  ControlToValidate="txtusernameo" ValidationGroup="vlpgDetailsUR"  
                    ID="RequiredFieldValidator323" runat="server"     ErrorMessage="Ingresar nombre de usuario" SetFocusOnError="True"></asp:RequiredFieldValidator>  
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="vlpgDetailsUR" ForeColor="Red" 
                    runat="server" ErrorMessage="Favor ingresar un email valido"  Font-Size="12px"   ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ControlToValidate="txtusernameo" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    <asp:TextBox ID="txtusernameo" ToolTip="Ingresar nombre de usuario"  Placeholder="usernameemail@mail.com"  class="form-control"  runat="server"></asp:TextBox> 

            
                    <asp:Label ID="Label12" runat="server"  Font-Bold="true" Text="Dirección*"></asp:Label>
                    <asp:RequiredFieldValidator    ForeColor="Red" Font-Size="11px"    ControlToValidate="txtaddress" ValidationGroup="vlpgDetails"  
                    ID="RequiredFieldValidator1" runat="server"   ErrorMessage="Ingresar una dirección" SetFocusOnError="True"></asp:RequiredFieldValidator>  
                    <asp:TextBox ID="txtaddress"  Placeholder="Dirección"  class="form-control"  runat="server" TextMode="MultiLine"></asp:TextBox> <br />
                     
                    <asp:Label ID="Label2"   runat="server" Text="Password:*"></asp:Label>                
                    <asp:RequiredFieldValidator  ForeColor="Red"  Font-Size="12px"  ControlToValidate="txtPassoword" ValidationGroup="vlpgDetailsUR"  
                    ID="RequiredFieldValidator12323" runat="server"      ErrorMessage="Favor ingresar una password" SetFocusOnError="True"></asp:RequiredFieldValidator>  
                    <asp:TextBox ID="txtPassoword"     TextMode="Password" class="form-control"  runat="server" ></asp:TextBox>             
  
                    <asp:Label ID="Label5"   runat="server" Text="Confirmar Password:*"></asp:Label>
                    <asp:RequiredFieldValidator  ForeColor="Red"  Font-Size="12px"  ControlToValidate="txtconfirmPassword" ValidationGroup="vlpgDetailsUR"  
                    ID="RequiredFieldValidator6" runat="server"      ErrorMessage="Favor ingresar una password" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtconfirmPassword"    TextMode="Password" class="form-control"  runat="server" ></asp:TextBox> 
                    <asp:Label ID="Label19" runat="server" Text="Ej: P@ssword123" Font-Size="9"></asp:Label>  <br /> <br />

                <asp:Button ID="btnSave" class="btn btn-primary btn-sm" runat="server"  ValidationGroup="vlpgDetailsUR"   Text="Crear Cuenta" OnClick="btnsave_Click"   /> &nbsp; &nbsp; &nbsp;
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Login_customer.aspx" Font-Underline="True" Font-Size="14px">Ya tengo una cuenta</asp:HyperLink>   <br />


                    <asp:CompareValidator ID="CompareValidator1" runat="server" ValidationGroup="vlpgDetailsUR" ForeColor="Red" 
                    ErrorMessage="Las password no coinciden" Font-Size="12px"  ControlToCompare="txtconfirmPassword"  ControlToValidate="txtPassoword" SetFocusOnError="True"></asp:CompareValidator>  <br /> 
                    
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Por favor ingrese al menos 8 caracteres" 
                    ControlToValidate="txtPassoword"  ValidationExpression="^(?=.*[a-z]{1}).{8,}$"  Font-Size="9" ForeColor="Red" SetFocusOnError="True"></asp:RegularExpressionValidator>

                  <br /> <asp:Label ID="lblmsg" runat="server"  ForeColor="Red" Text=""></asp:Label>
                  
        </div>

    <div class="col-md-5"> 
         <ul>
            <li><b>Ordena en nuestr sitio</b></li> 
            <li><b>Gestiona los pedidos</b></li> 
            <li><b>Método de pago seguro</b></li>  
        </ul>
    </div>
</div>
</asp:Content>

